import bcrypt from "bcryptjs";

const users = [
    {
        name: "Admin User",
        email: "admin@example.com",
        password: bcrypt.hashSync("123456", 10),
        isAdmin: true,
    },
    {
        name: "hidayat",
        email: "hidayat@example.com",
        password: bcrypt.hashSync("123456", 10),
    },
    {
        name: "hidayah",
        email: "hidayah@example.com",
        password: bcrypt.hashSync("123456", 10),
    },
];

export default users;
