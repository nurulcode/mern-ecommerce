import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col } from "react-bootstrap";
import Product from "../components/Product";
import { listProducts } from "../actions/productActions";
import Loader from "../components/Loader";
import InfiniteScroll from "react-infinite-scroll-component";

const HomeScreen = ({ match }) => {
    const keyword = match.params.keyword;

    const [limit] = useState(12);
    const [offset, setOffset] = useState(0);

    const dispatch = useDispatch();

    const productList = useSelector((state) => state.productList);
    const { products, hasMore } = productList;

    useEffect(() => {
        dispatch(listProducts(keyword, limit, offset));
    }, [dispatch, keyword, limit, offset]);

    const fetchMore = () => {
        setOffset((offset) => offset + limit);
    };

    return (
        <>
            <h1>Latest Products</h1>
            <InfiniteScroll
                dataLength={products.length}
                next={fetchMore}
                hasMore={hasMore}
                loader={<Loader />}
                style={{ overflowY: 'hidden !important' }}>
                <Row>
                    {products.map((product, index) => (
                        <Col key={index} sm={12} md={6} lg={4} xl={3}>
                            <Product product={product} />
                        </Col>
                    ))}
                </Row>
            </InfiniteScroll>
        </>
    );
};

export default HomeScreen;
