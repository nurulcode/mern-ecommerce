import React, { useEffect } from "react";
import { Button, Row, Col, Table } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import Message from "../components/Message";
import Loader from "../components/Loader";
import { listProducts, deleteProduct, createProduct } from "../actions/productActions";
import NumberFormat from "react-number-format";
import { PRODUCT_CREATE_RESET } from "../constants/productConstants";

const ProductListScreen = ({ history }) => {
    const dispatch = useDispatch();

    const productList = useSelector((state) => state.productList);
    const { loading, products, error } = productList;

    const userLogin = useSelector((state) => state.userLogin);
    const { userInfo } = userLogin;

    const productDelete = useSelector((state) => state.productDelete);
    const {
        success: successDelete,
        error: errorDelete,
        loading: loadingDelete,
    } = productDelete;

    const productCreate = useSelector((state) => state.productCreate);
    const {
        success: successCreate,
        error: errorCreate,
        loading: loadingCreate,
        product: createdProduct,
    } = productCreate;

    useEffect(() => {
        dispatch({
            type: PRODUCT_CREATE_RESET,
        });

        if (!userInfo.isAdmin) {
            history.push("/login");
        }
        if (successCreate) {
            history.push(`/admin/product/${createdProduct._id}/edit`);
        } else {
            dispatch(listProducts("", 10, 0));
        }
    }, [
        dispatch,
        history,
        userInfo,
        successDelete,
        successCreate,
        createdProduct,
    ]);

    const deleteHandler = (id) => {
        if (window.confirm("Are you sure ? ")) {
            dispatch(deleteProduct(id));
        }
    };

    const createProductHandler = () => {
        dispatch(createProduct());
    };

    return (
        <>
            <Row className="my-3">
                <Col>
                    <h2>List Products</h2>
                </Col>
                <Col className="text-right">
                    <Button onClick={createProductHandler}>
                        <i className="fas fa-plus"></i> Create Product
                    </Button>
                </Col>
            </Row>

            {loading || loadingDelete || loadingCreate ? (
                <Loader />
            ) : error ? (
                <Message variant="danger">{error}</Message>
            ) : errorDelete ? (
                <Message variant="danger">{errorDelete}</Message>
            ) : errorCreate ? (
                <Message variant="danger">{errorCreate}</Message>
            ) : (
                <Table striped bordered hover responsive className="table-sm">
                    <thead>
                        <tr className="text-center">
                            <th>ID</th>
                            <th>NAME</th>
                            <th>PRICE</th>
                            <th>CATEGORY</th>
                            <th>BRAND</th>
                            <th>ACTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        {products.map((product, index) => (
                            <tr
                                key={index}
                                className="justify-content-center"
                            >
                                <td>{product._id}</td>
                                <td>{product.name}</td>
                                <td>
                                    <NumberFormat
                                        value={product.price}
                                        displayType={"text"}
                                        thousandSeparator={true}
                                        prefix={"Rp. "}
                                    />
                                </td>
                                <td>{product.category}</td>
                                <td>{product.brand}</td>
                                <td>
                                    <LinkContainer
                                        to={`/admin/product/${product._id}/edit`}
                                    >
                                        <Button
                                            variant="info"
                                            className="btn-sm"
                                        >
                                            <i className="fas fa-edit"></i>
                                        </Button>
                                    </LinkContainer>
                                    <Button
                                        variant="danger"
                                        className="btn-sm ml-1"
                                        onClick={() =>
                                            deleteHandler(product._id)
                                        }
                                    >
                                        <i className="fas fa-trash"></i>
                                    </Button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            )}
        </>
    );
};

export default ProductListScreen;
